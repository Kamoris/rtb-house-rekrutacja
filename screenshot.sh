#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

docker run --rm \
    -v ${SCRIPT_DIR}:/app \
    alekzonder/puppeteer:latest \
    node app.js