"use strict";

const puppeteer = require('puppeteer');
const readline = require('readline');
const fs = require('fs');

(function () {
    main({
        inputFile: 'params.txt',
        outputFile: 'screen.png',
        viewport: {
            width: 1920,
            height: 1080
        },
        adDomain: 'creativecdn.com'
    });
})();

function main(config) {
    processInput(config.inputFile, async (tagLinks, screenLink) => {
        const browser = await puppeteer.launch({
            args: ['--no-sandbox', '--disable-setuid-sandbox']
        });
        const page = await browser.newPage();

        await processTagging(page, tagLinks);
        await ensureAdDisplay(page, screenLink, config.adDomain);

        console.log('Taking screenshot...');
        page.setViewport(config.viewport);
        await page.screenshot({path: config.outputFile});

        await browser.close();
    });
}

function processInput(inputFile, callback) {
    console.log('Reading input...');
    const input = fs.createReadStream(inputFile);
    const rl = readline.createInterface({
        input: input,
        output: null,
        console: false
    });

    const links = [];
    rl.on('line', (link) => {
        links.push(link);
    });
    rl.on('close', () => {
        const screenLink = links.pop();
        callback(links, screenLink);
    });
}

async function processTagging(page, tagLinks) {
    for (let link of tagLinks) {
        console.log('Tagging... (' + link + ')');
        await page.goto(link, {
            waitUntil: 'networkidle2'
        });
    }
}

async function ensureAdDisplay(page, screenLink, adDomain) {
    let adLoaded = false;
    page.on('request', (request) => {
        if (request.url.includes(adDomain)) {
            adLoaded = true;
        }
    });

    console.log('Loading screenshot page... (' + screenLink + ')');
    await page.goto(screenLink, {
        waitUntil: 'networkidle0'
    });

    let requestLimit = 5;
    for (let i = 0; i < requestLimit; i++) {
        console.log('Checking if ad was loaded...');
        if (adLoaded) {
            return;
        }
        console.log('Reloading screenshot page...');
        await page.reload({
            waitUntil: 'networkidle0'
        });
    }
}
